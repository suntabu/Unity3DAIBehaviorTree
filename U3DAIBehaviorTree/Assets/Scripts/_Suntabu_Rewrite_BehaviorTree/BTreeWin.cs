﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using UnityEditor;

namespace Inlycat.AI.BehaviorTree
{
#if UNITY_EDITOR
    public class BTreeWin : EditorWindow
    {
        public static int NODE_WIDTH = 20;
        public static int NODE_HEIGHT = 20;
        public static int GUI_WIDTH = 240;

        public static BTree CurrentTree;
        public static BNode CurrentNode;
        public static BTreeWin Instance = null;

        private static int current_tree_index = -1;
        private static int last_tree_index = -1;
        private static int select_create_node_id = -1;

        public static BNode Select;

        // temp value
        private Vector2 m_CScrollPos = new Vector2();
        private string m_StrInputName = "";

        [MenuItem("Inlycat/AI/Behavior Tree Editor")]
        static void InitWin()
        {
            BTreeWin window = GetWindow<BTreeWin>();
            Instance = window;
        }

        public static void AddNode(object arg)
        {
            Debug.Log("callback " + arg);
        }

        void OnGUI()
        {
            this.m_CScrollPos = GUI.BeginScrollView(new Rect(0, 0, position.width - 240, position.height), this.m_CScrollPos, new Rect(0, 0, this.maxSize.x, this.maxSize.y));

            Texture2D tex1 = new Texture2D(1, 1);
            tex1.SetPixel(0, 0, Color.gray);
            tex1.Apply();

            Texture2D tex2 = new Texture2D(1, 1);
            tex2.SetPixel(0, 0, Color.white);
            tex2.Apply();

            for (int i = 0; i < 1000; i++)
            {
                if (i % 2 == 0)
                {
                    GUI.DrawTexture(new Rect(0, i * NODE_HEIGHT, BTreeWin.Instance.position.width, NODE_HEIGHT), tex1);
                }
                else
                {
                    GUI.DrawTexture(new Rect(0, i * NODE_HEIGHT, BTreeWin.Instance.position.width, NODE_HEIGHT), tex2);
                }
            }

            if (CurrentTree != null && CurrentTree.Root != null)
            {
                int xx = 0;
                int yy = 0;
                CurrentTree.Root.Render(xx, ref yy);
            }
            GUI.EndScrollView();

            /************** draw the editor gui ********************/
            GUI.BeginGroup(new Rect(position.width - GUI_WIDTH, 0, 300, 1000));
            int x = 0;
            int y = 0;
            List<BTree> list = BTreeManager.Instance.GetTrees();
            if (GUI.Button(new Rect(x, y, 200, 40), "Load..."))
            {
                CurrentTree = null;
                CurrentNode = null;
                current_tree_index = -1;
                last_tree_index = -1;
                select_create_node_id = -1;
                Select = null;
                BTreeManager.Instance.EditorLoad();
            }

            y += 40;
            if (GUI.Button(new Rect(x, y, 200, 40), "Save Editor BTree..."))
            {
                BTreeManager.Instance.EditorSave();
                AssetDatabase.Refresh();
            }
            y += 40;
            if (GUI.Button(new Rect(x, y, 200, 40), "Save Tree..."))
            {
                //BTreeManager.Instance.SaveEx();
                AssetDatabase.Refresh();
            }

            y += 40;
            GUI.Label(new Rect(x, y, 200, 20), "===============================");
            y += 20;

            this.m_StrInputName = GUI.TextField(new Rect(x, y + 10, 100, 29), this.m_StrInputName);
            if (GUI.Button(new Rect(x + 100, y, 100, 40), "Create Tree..."))
            {
                if (this.m_StrInputName != string.Empty)
                {
                    CurrentNode = null;
                    BTree tree = new BTree()
                    {
                        m_StrName = this.m_StrInputName
                    };

                    BTreeManager.Instance.Add(tree);

                    list = BTreeManager.Instance.GetTrees();
                    CurrentTree = tree;
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i].m_StrName == tree.m_StrName)
                        {
                            current_tree_index = i;
                            break;
                        }
                    }
                    last_tree_index = current_tree_index;
                    Repaint();
                }
            }

            y += 40;
            if (GUI.Button(new Rect(x, y, 200, 40), "Remove Tree"))
            {
                CurrentNode = null;
                BTreeManager.Instance.Remove(CurrentTree);
                list = BTreeManager.Instance.GetTrees();
                CurrentTree = null;
                current_tree_index = -1;
                last_tree_index = -1;
                Repaint();
            }

            y += 40;
            GUI.Label(new Rect(x, y, 200, 20), "=========================");
            y += 20;

            string[] treeNames = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                treeNames[i] = list[i].m_StrName;
            }

            current_tree_index = EditorGUI.Popup(new Rect(x, y, 200, 45), current_tree_index, treeNames);
            if (current_tree_index != last_tree_index)
            {
                last_tree_index = current_tree_index;
                CurrentTree = list[current_tree_index];
                CurrentNode = null;
            }

            y += 40;
            GUI.Label(new Rect(x, y, 200, 20), "==========================");
            y += 20;

            if (CurrentTree != null)
            {
                GUI.Label(new Rect(x, y, 200, 20), "Tree Name:" + CurrentTree.m_StrName);
                y += 20;
                CurrentTree.m_StrName = GUI.TextField(new Rect(x, y, 100, 20), CurrentTree.m_StrName);
                y += 20;
            }

            select_create_node_id = EditorGUI.Popup(new Rect(x, y, 100, 40), select_create_node_id, BNodeFactory.Instance.GetNodeList());

            if (GUI.Button(new Rect(x + 100, y, 100, 40), "Create Root..."))
            {
                if (select_create_node_id >= 0)
                {
                    BNode node = BNodeFactory.Instance.Create(select_create_node_id);
                    if (CurrentTree != null)
                    {
                        CurrentTree.Root = node;
                    }
                }
            }

            y += 40;
            if (GUI.Button(new Rect(x, y, 200, 40), "Clear"))
            {
                if (CurrentTree != null)
                {
                    CurrentTree.Clear();
                }
            }

            y += 40;
            GUI.Label(new Rect(x, y, 200, 20), "==========================");
            y += 20;
            if (CurrentNode != null)
            {
                GUI.Label(new Rect(x, y, 300, 20), "Node Type: " + CurrentTree.GetType().FullName);
                y += 20;
                GUI.Label(new Rect(x, y, 200, 20), "Node Name: " + CurrentNode.GetName());
                y += 20;
                GUI.Label(new Rect(x, y, 200, 15), "==================================");
                y += 15;
                CurrentNode.RenderEditor(x, y);
            }
            GUI.EndGroup();
        }

        void Update()
        {
            Instance = this;
            if (Select != null)
            {
                Repaint();
            }
        }

    }

#endif
}
