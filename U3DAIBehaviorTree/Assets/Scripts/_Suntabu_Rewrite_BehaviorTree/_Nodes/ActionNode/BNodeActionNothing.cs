﻿using UnityEngine;
using System.Collections;


namespace Inlycat.AI.BehaviorTree
{
    public class BNodeActionNothing : BNodeAction
    {
        public int m_iTest;

        public BNodeActionNothing()
            : base()
        {
            this.m_StrName = "BNodeActionNothing";
        }

        public override ActionResult Execute(BInput input)
        {
            return ActionResult.SUCCESS;
        }

        //		public override void DrawGUI(int x , int y)
        //		{
        //			try
        //			{
        //				GUI.Label(new Rect(x,y,100,30) ,"test condition");
        //				y+=30;
        //				string tmpint = "" + this.m_iTest;
        //				tmpint = GUI.TextField(new Rect(x,y,100,30) , tmpint);
        //				this.m_iTest = int.Parse(tmpint);
        //			}
        //			catch( Exception ex )
        //			{
        //				Debug.Log(ex.StackTrace);
        //			}
        //		}
    }


}
