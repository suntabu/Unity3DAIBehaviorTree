﻿using UnityEngine;
using System.Collections;

namespace Inlycat.AI.BehaviorTree
{

    public class BNodeRandom : BNodeComposite
    {
        private int m_iRunningIndex;

        public BNodeRandom() : base()
        {
            this.m_StrName = "Random";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = Random.Range(0, this.m_ListChildren.Count);
            base.OnEnter(input);
        }

        public override ActionResult Execute(BInput input)
        {
            return this.m_ListChildren[this.m_iRunningIndex].RunNode(input);
        }
    }   

}
