﻿using UnityEngine;
using System.Collections;

namespace Inlycat.AI.BehaviorTree
{
    public class BNodeIterator : BNodeComposite
    {
        public int Num;

        private int m_iRunningIndex;
        private int m_iRunningNum;

        public BNodeIterator() : base()
        {
            this.m_StrName = "Iterator";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = 0;
            this.m_iRunningNum = 0;
        }

        public override ActionResult Execute(BInput input)
        {
            if (this.m_iRunningIndex >=this.m_ListChildren.Count)
            {
                return ActionResult.FAILURE;
            }

            ActionResult result = this.m_ListChildren[this.m_iRunningIndex].RunNode(input);

            if (result == ActionResult.FAILURE)
            {
                return ActionResult.FAILURE;
            }

            if (result == ActionResult.SUCCESS)
            {
                this.m_iRunningIndex ++;
                this.m_iRunningNum ++;
            }

            if (this.m_iRunningNum >= this.Num)
            {
                return ActionResult.SUCCESS;
            }
            return ActionResult.RUNNING;
        }
    }


}
