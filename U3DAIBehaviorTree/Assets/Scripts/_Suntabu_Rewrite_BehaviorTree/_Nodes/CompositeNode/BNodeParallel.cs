﻿using UnityEngine;
using System.Collections;

namespace Inlycat.AI.BehaviorTree
{

    /// <summary>
    /// 并行节点
    /// </summary>
    public class BNodeParallel : BNodeComposite
    {
        private int m_iRunningIndex;

        public BNodeParallel()
            : base()
        {
            this.m_StrName = "Parallel";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = 0;
        }

        public override ActionResult Execute(BInput input)
        {
            if (this.m_iRunningIndex >= this.m_ListChildren.Count)
            {
                return ActionResult.SUCCESS;
            }

            BNode node = this.m_ListChildren[this.m_iRunningIndex];
            ActionResult res = node.RunNode(input);
            if (res != ActionResult.RUNNING)
            {
                this.m_iRunningIndex++;
            }
            return ActionResult.RUNNING;

        }
    }

}
