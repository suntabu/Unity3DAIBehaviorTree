﻿using UnityEngine;
using System.Collections;

namespace Inlycat.AI.BehaviorTree
{
    public class BNodeParallelAllFail : BNodeParallel
    {
        private int m_iRunningIndex;
        private ActionResult m_eResult;

        public BNodeParallelAllFail():base()
        {
            this.m_StrName = "ParallelAllFail";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = 0;
            this.m_eResult = ActionResult.SUCCESS;
        }

        public override ActionResult Execute(BInput input)
        {
            if (this.m_iRunningIndex >= this.m_ListChildren.Count)
            {
                return this.m_eResult;
            }

            ActionResult result = this.m_ListChildren[this.m_iRunningIndex].RunNode(input);
            if (result == ActionResult.SUCCESS)
            {
                this.m_eResult = ActionResult.FAILURE;
            }

            if (result != ActionResult.RUNNING)
            {
                this.m_iRunningIndex++;
            }

            return ActionResult.FAILURE;
        }
    }


}
