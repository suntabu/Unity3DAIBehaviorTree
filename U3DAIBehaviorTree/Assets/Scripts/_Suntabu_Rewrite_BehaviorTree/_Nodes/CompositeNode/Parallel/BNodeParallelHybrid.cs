﻿using UnityEngine;
using System.Collections;


namespace Inlycat.AI.BehaviorTree
{
    public class BNodeParallelHybrid : BNodeParallel
    {
        public bool SuccessOrFailure;
        public int Num;

        private int m_iRunningIndex;
        private int m_iSuccessNum;

        public BNodeParallelHybrid() : base()
        {
            this.m_StrName = "ParallelHybrid";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = 0;
            this.m_iSuccessNum = 0;
        }

        public override ActionResult Execute(BInput input)
        {
            if (this.m_iRunningIndex >=this.m_ListChildren.Count)
            {
                if (this.SuccessOrFailure)
                {
                    if (this.Num <= this.m_iSuccessNum)
                    {
                        return ActionResult.SUCCESS;
                    }
                }
                else
                {
                    if (this.Num <= (this.m_ListChildren.Count - this.m_iSuccessNum))
                    {
                        return ActionResult.SUCCESS;
                    }
                }
                return ActionResult.FAILURE;
            }

            ActionResult result = this.m_ListChildren[this.m_iRunningIndex].RunNode(input);
            if (result == ActionResult.SUCCESS)
            {
                this.m_iSuccessNum++;
            }
            if (result != ActionResult.RUNNING)
            {
                this.m_iRunningIndex++;
            }

            return ActionResult.RUNNING;
        }
    }


}
