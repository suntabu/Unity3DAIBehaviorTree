﻿using UnityEngine;
using System.Collections;

namespace Inlycat.AI.BehaviorTree
{
    public class BNodeSelector : BNodeComposite
    {
        private int m_iRunningIndex;

        public BNodeSelector()
            : base()
        {
            this.m_StrName = "Selector";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = 0;
        }

        public override ActionResult Execute(BInput input)
        {
            if (this.m_iRunningIndex >= this.m_ListChildren.Count)
            {
                return ActionResult.FAILURE;
            }

            BNode node = this.m_ListChildren[this.m_iRunningIndex];
            ActionResult result = node.RunNode(input);

            if (result == ActionResult.SUCCESS)
            {
                return ActionResult.SUCCESS;
            }

            if (result == ActionResult.FAILURE)
            {
                this.m_iRunningIndex++;
            }
            return ActionResult.RUNNING;
        }
    }


}
