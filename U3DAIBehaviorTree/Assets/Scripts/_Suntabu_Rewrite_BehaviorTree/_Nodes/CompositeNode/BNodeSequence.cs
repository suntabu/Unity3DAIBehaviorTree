﻿using UnityEngine;
using System.Collections;


namespace Inlycat.AI.BehaviorTree
{
    public class BNodeSequence : BNodeComposite
    {
        private int m_iRunningIndex;

        public BNodeSequence() : base()
        {
            this.m_StrName = "Sequence";
        }

        public override void OnEnter(BInput input)
        {
            this.m_iRunningIndex = 0;
        }

        public override ActionResult Execute(BInput input)
        {
            if (this.m_iRunningIndex >= this.m_ListChildren.Count)
            {
                return ActionResult.SUCCESS;
            }

            BNode node = this.m_ListChildren[this.m_iRunningIndex];
            ActionResult result = node.RunNode(input);

            if (result == ActionResult.FAILURE)
            {
                return ActionResult.FAILURE;
            }

            if (result == ActionResult.SUCCESS)
            {
                this.m_iRunningIndex++;
            }

            return ActionResult.RUNNING;
        }
    }


}
