﻿using UnityEngine;
using System.Collections;

namespace Inlycat.AI.BehaviorTree
{

    public class BNodeComposite : BNode
    {
        public BNodeComposite() : base()
        {
            this.m_StrName = "Composite";
        }
    }

}
