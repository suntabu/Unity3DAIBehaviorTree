﻿using System;
using UnityEngine;
using System.Collections;
using Game.AIBehaviorTree;
using LitJson;

namespace Inlycat.AI.BehaviorTree
{
    public class BTree
    {
        public string m_StrName;
        public BNode Root;

        public BTree(){}

        public void WriteJson(JsonData parent)
        {
            JsonData json = new JsonData();
            json["name"] = this.m_StrName;
            if (this.Root != null)
            {
                json["node"] = new JsonData();
                json["node"].SetJsonType(JsonType.Object);
                json["node"] = this.Root.WriteJson();
            }
            parent.Add(json);
        }

        public void ReadJson(JsonData json)
        {
            this.m_StrName = json["name"].ToString();
            this.Root = null;
            if (json.Keys.Contains("node"))
            {
                string typeName = json["node"]["type"].ToString();
                Type t = Type.GetType(typeName);
                this.Root = Activator.CreateInstance(t) as BNode;
                this.Root.ReadJson(json["node"]);
            }
        }

        public void SetRoot(BNode node)
        {
            this.Root = node;
        }

        public void Clear()
        {
            this.Root = null;
        }

        public void Run(BInput input)
        {
            this.Root.RunNode(input);
        }

    }


}

