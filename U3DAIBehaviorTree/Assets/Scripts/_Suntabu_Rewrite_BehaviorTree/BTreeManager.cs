﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using UnityEditor;

namespace Inlycat.AI.BehaviorTree
{
    public class BTreeManager
    {
        public Dictionary<string, BTree> m_MapTree = new Dictionary<string, BTree>();

        private static BTreeManager s_CInstance;

        public static BTreeManager Instance
        {
            get
            {
                if (s_CInstance == null)
                {
                    s_CInstance = new BTreeManager();
                }
                return s_CInstance;
            }
        }

        public BTreeManager() { }

        public void Load(string jsonText)
        {
            JsonData json = JsonMapper.ToObject(jsonText);
            json = json["trees"];
            int count = json.Count;
            for (int i = 0; i < count; i++)
            {
                BTree bt = new BTree();
                JsonData data = json[i];
                bt.ReadJson(data);
                this.m_MapTree.Add(bt.m_StrName, bt);
            }
        }


        public BTree GetTree(string name)
        {
            if (this.m_MapTree.ContainsKey(name))
            {
                return this.m_MapTree[name];
            }
            return null;
        }

        public List<BTree> GetTrees()
        {
            var list = new List<BTree>();
            foreach (var tree in m_MapTree.Values)
            {
                list.Add(tree);
            }
            return list;
        }

        public void Add(BTree tree)
        {
            if (this.m_MapTree.ContainsKey(tree.m_StrName))
            {
#if UNITY_EDITOR
                UnityEditor.EditorUtility.DisplayDialog("Error",
                    "The tree named " + tree.m_StrName + " is already existed", "ok");

#endif
                return;
            }
            this.m_MapTree.Add(tree.m_StrName, tree);
        }

        public void Remove(BTree tree)
        {
            if (tree == null)
            {
                return;
            }

            if (this.m_MapTree.ContainsKey(tree.m_StrName))
            {
                this.m_MapTree.Remove(tree.m_StrName);
            }
            else
            {
                Debug.LogError("The tree id is not existed");
            }
        }


#if UNITY_EDITOR

        public void EditorSave()
        {
            string filePath = EditorUtility.SaveFilePanel("Behavior Tree", Application.dataPath, "", "json");
            Debug.Log(filePath);
            JsonData data = new JsonData();
            data["trees"] = new JsonData();
            data["trees"].SetJsonType(JsonType.Array);
            foreach (var bTree in m_MapTree)
            {
                bTree.Value.WriteJson(data["trees"]);
            }

            File.WriteAllText(filePath, data.ToJson());
        }


        public void EditorLoad()
        {
            string filePath = EditorUtility.OpenFilePanel("Behavior Tree", Application.dataPath, "json");
            if (filePath == "")
            {
                return;
            }

            this.m_MapTree.Clear();

            string txt = File.ReadAllText(filePath);
            Load(txt);
        }
#endif

    }


}
