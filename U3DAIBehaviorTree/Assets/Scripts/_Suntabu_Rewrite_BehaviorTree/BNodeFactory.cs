﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO; 
using UnityEngineInternal;

namespace Inlycat.AI.BehaviorTree
{
    public class BNodeFactory
    {
        public List<Type> m_ListComposite = new List<Type>();
        public List<Type> m_ListAction = new List<Type>();
        public List<Type> m_ListCondition = new List<Type>();
        public List<Type> m_ListDecorator = new List<Type>();

        private static BNodeFactory s_CInstance;
        public static BNodeFactory Instance
        {
            get
            {
                if (s_CInstance == null)
                {
                    s_CInstance = new BNodeFactory();
                }
                return s_CInstance;
            }
        }

        public BNodeFactory()
        {
            this.m_ListComposite = GetSubClass(typeof(BNodeComposite));
            this.m_ListAction = GetSubClass(typeof(BNodeAction));
            this.m_ListCondition = GetSubClass(typeof(BNodeCondition));
            this.m_ListDecorator = GetSubClass(typeof(BNodeDecorator));
        }

        public BNode Create(int index)
        {
            if (this.m_ListComposite.Count > index)
            {
                Type t = this.m_ListComposite[index];
                BNode node = Activator.CreateInstance(t) as BNode;
                return node;
            }
            Debug.LogError("The type index is none : " + index);
            return null;
        }

        public string[] GetNodeList()
        {
            string[] str = new string[this.m_ListComposite.Count];
            for (int i = 0; i < this.m_ListComposite.Count; i++)
            {
                Type item = this.m_ListComposite[i];
                str[i] = item.Name;
            }
            return str;
        }


#if UNITY_EDITOR
        public static List<Type> GetSubClass(Type type)
        {
            List<Type> listType = new List<Type>();

            string rootPath = Application.dataPath + "/";

            List<string> files = GetAllFiles(rootPath);
            foreach (var file in files)
            {
                string className = file.Split('.')[0];
                Type tt = Type.GetType("Inlycat.AI.BehaviorTree." + className);
                if (tt != null)
                {
                    if (tt.IsSubclassOf(type))
                    {
                        listType.Add(tt);
                    }
                }
            }
            return listType;
        }

        private static List<string> GetAllFiles(string dir)
        {
            List<string> list = new List<string>();
            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            foreach (var fileInfo in dirInfo.GetFiles("*.cs"))
            {
                list.Add(fileInfo.Name);
            }

            string[] dirs = Directory.GetDirectories(dir);
            for (int i = 0; i < dirs.Length; i++)
            {
                string path = dirs[i];

                list.AddRange(GetAllFiles(path));

            }
            return list;
        }


#endif


    }
}

